#include <iostream>

using namespace std;

int main ()

{
        char operation;
        double firstoperand, secondoperand;
        double result;

        cout << "Enter operation (a;s;m;p) : " << endl ;
        cin >> operation ;
        cout << "Enter firstoperand : " << endl ;
        cin >> firstoperand ;
        cout << "Enter secondoperand : " << endl ;
        cin >> secondoperand ;


        if(operation == 'a')
        {
                result = firstoperand + secondoperand ;
                cout << firstoperand << " + " << secondoperand << " = " << result << endl;
        }
        else if (operation == 's' )
        {
                result = firstoperand - secondoperand ;
                cout << firstoperand << " - " << secondoperand << " = " << result << endl;
        }
        else if (operation == 'm' )
        {
                result = firstoperand * secondoperand ;
                cout << firstoperand << " * " << secondoperand << " = " << result << endl;
        }
        else if (operation == 'p' )
        {
                result = firstoperand / secondoperand ;
                cout << firstoperand << " / " << secondoperand << " = " << result << endl;
        }
        return 0;
}
